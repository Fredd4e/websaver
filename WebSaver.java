//Fredrik Carlsson

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

//Standard
public class WebSaver {
	private String pageURL;
	private String fileName;
	
	public WebSaver(String pageURL, String fileName){
		this.pageURL = pageURL;
		this.fileName = fileName;
	}
	
	public void fetchAndSave() throws IOException{
	
		//URL sk�ter kontakten med http servern.
		URL tofetch = new URL(pageURL);
		//openStream �ppnar en str�m med websidans kod, Buffered g�r s� vi slipper kontakta http servn varje g�ng vi 
		//vill l�sa data.
		BufferedReader in = new BufferedReader(new InputStreamReader(tofetch.openStream()));
		
		
		File file = new File(fileName);
		//�ppnar fil f�r skrivning.
		BufferedWriter out = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
		
		//kollar s� filen finns om inte, skapa.
		if(!file.exists())
			file.createNewFile();
		
		//skriver data char f�r char till filen fr�n str�mmen.
		int inputData;
		while((inputData = in.read()) != -1){
			out.write(inputData);
		}
		out.close();
		in.close();
		
	}
	
	//Metod f�r att l�sa in, hitta, och skriva ut l�nkar.
	public void printLinks() throws IOException{
		File file = new File(fileName);
		ArrayList<String> list = new ArrayList<String>();
		
		BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()));
		
		//link �r tempor�r lagrings plats f�r inl�st str�ng.
		String link;
		
		//Denna loop l�ser in en linje fr�n filen, unders�ker om a href finns i linjen & 
		//och tar is�fall och delar upp med delimitern(") och lagrar andra token i ArrayListan.
		while((link = in.readLine()) != null){
			if(link.toLowerCase().contains("a href")){
				String delimiter = "[\"]";
				String[] tokenized = link.split(delimiter);
				list.add(tokenized[1]);
			}
		}
		
		in.close();
		//Skriver ut listans inneh�ll i konsolen!
		if(!list.isEmpty()){
			for (int i = 0; i < list.size(); i++) {
				System.out.println(list.get(i));
			}
		}
	}
}
