//Fredrik Carlsson

import java.net.*;
import java.io.*;
		//Start class med main.
public class Start {

	public static void main(String[] args) throws Exception {
		WebSaver webSave = new WebSaver("http://arthead.se/java14/uppgift1.html", "uppgift1.html");
		webSave.fetchAndSave();
		WebSaver webSave2 = new WebSaver("http://arthead.se/java14/uppgift2.html", "uppgift2.html");
		webSave2.fetchAndSave();
		//Kalla p� andra metoden f�r att h�mta och skriva ut l�nkarna i konsolen.
		webSave2.printLinks();
	}
	
	
}
